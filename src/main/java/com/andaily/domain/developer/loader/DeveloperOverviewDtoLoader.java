package com.andaily.domain.developer.loader;

import com.andaily.domain.developer.*;
import com.andaily.domain.developer.burndown.BurnDown;
import com.andaily.domain.developer.burndown.BurnDownGenerator;
import com.andaily.domain.dto.developer.*;
import com.andaily.domain.shared.paginated.PaginatedLoader;
import com.andaily.domain.shared.security.SecurityUtils;
import com.andaily.domain.user.ScrumTerm;
import com.andaily.web.context.BeanProvider;
import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Date: 13-8-8
 *
 * @author Shengzhao Li
 */
public class DeveloperOverviewDtoLoader {

    private transient SprintRepository sprintRepository = BeanProvider.getBean(SprintRepository.class);
    private DeveloperOverviewDto overviewDto;

    public DeveloperOverviewDtoLoader(DeveloperOverviewDto overviewDto) {
        this.overviewDto = overviewDto;
    }

    public DeveloperOverviewDto load() {
        loadAvailableSprints();
        loadCurrentSprint();
        loadTasks();

        loadTotalTasks();
        loadBurnDown();
        loadLatestMeetings();
        return overviewDto;
    }

    private void loadLatestMeetings() {
        if (!hasCurrentSprintDto()) {
            return;
        }
        String sprintGuid = overviewDto.getCurrentSprint().getGuid();
        List<SprintMeeting> meetings = sprintRepository.findLatestMeetings(sprintGuid, 5);
        overviewDto.setLatestMeetings(SprintMeetingDto.toDtos(meetings));
    }

    private void loadBurnDown() {
        if (!hasCurrentSprintDto()) {
            return;
        }
        String sprintGuid = overviewDto.getCurrentSprint().getGuid();
        Sprint sprint = sprintRepository.findByGuid(sprintGuid);

        BurnDownGenerator burnDownGenerator = new BurnDownGenerator(sprint);
        BurnDown burnDown = burnDownGenerator.generate();
        overviewDto.setBurnDownWrapper(new BurnDownChartWrapper(burnDown));
    }

    private void loadTotalTasks() {
        if (!hasCurrentSprintDto()) {
            return;
        }
        String sprintGuid = overviewDto.getCurrentSprint().getGuid();
        overviewDto.setTotalCreatedTasks(sprintRepository.totalTasksBySprintAndStatus(sprintGuid, SprintTaskStatus.CREATED));
        overviewDto.setTotalPendingTasks(sprintRepository.totalTasksBySprintAndStatus(sprintGuid, SprintTaskStatus.PENDING));
        overviewDto.setTotalFinishedTasks(sprintRepository.totalTasksBySprintAndStatus(sprintGuid, SprintTaskStatus.FINISHED));
        overviewDto.setTotalCanceledTasks(sprintRepository.totalTasksBySprintAndStatus(sprintGuid, SprintTaskStatus.CANCELED));
    }

    private void loadTasks() {
        final Map<String, Object> map = overviewDto.queryParams();
        overviewDto = overviewDto.load(new PaginatedLoader<SprintTaskDto>() {
            @Override
            public List<SprintTaskDto> loadList() {
                List<SprintTask> tasks = sprintRepository.findSprintTasks(map);
                return SprintTaskDto.toDtos(tasks);
            }

            @Override
            public int loadTotalSize() {
                return sprintRepository.totalSprintTasks(map);
            }
        });
    }

    private void loadAvailableSprints() {
        Map<String, Object> map = new HashMap<>();
        map.put("currUser", SecurityUtils.currUser());
        // Different role will call different limit
        final ScrumTerm scrumTerm = SecurityUtils.currUser().scrumTerm();
        map.put("isProductOwner", scrumTerm.equals(ScrumTerm.PRODUCT_OWNER) ? "yes" : null);
        map.put("isTeamMember", (scrumTerm.isMaster() || scrumTerm.isMember()) ? "yes" : null);


        List<Sprint> sprints = sprintRepository.findAvailableSprints(map);
        List<SprintSimpleDto> sprintDtos = overviewDto.getSprintDtos();
        for (Sprint sprint : sprints) {
            final SprintSimpleDto simpleDto = new SprintSimpleDto(sprint);
            sprintDtos.add(simpleDto);
        }
    }


    private void loadCurrentSprint() {
        if (hasCurrentSprintDto()) {
            String guid = overviewDto.getCurrentSprint().getGuid();
            loadAndSetCurrentSprint(guid);
        } else {
            final SprintSimpleDto defaultSprint = findDefaultSprint();
            if (defaultSprint != null) {
                loadAndSetCurrentSprint(defaultSprint.getGuid());
            }
        }
    }

    private SprintSimpleDto findDefaultSprint() {
        List<SprintSimpleDto> sprintDtos = overviewDto.getSprintDtos();
        SprintSimpleDto sprintSimpleDto = null;
        for (SprintSimpleDto sprintDto : sprintDtos) {
            if (sprintDto.isDefaultSprint()) {
                sprintSimpleDto = sprintDto;
                break;
            }
        }
        if (sprintSimpleDto == null && !sprintDtos.isEmpty()) {
            sprintSimpleDto = sprintDtos.get(0);
        }
        return sprintSimpleDto;
    }

    private boolean hasCurrentSprintDto() {
        SprintDto sprintDto = overviewDto.getCurrentSprint();
        return sprintDto != null && StringUtils.isNotEmpty(sprintDto.getGuid());
    }

    private void loadAndSetCurrentSprint(String guid) {
        Sprint currentSprint = sprintRepository.findByGuid(guid);
        overviewDto.setCurrentSprint(new SprintDto(currentSprint));
    }
}
