package com.andaily.service;

import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * @author Shengzhao Li
 */

public interface SecurityService extends UserDetailsService {
}